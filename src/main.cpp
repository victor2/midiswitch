#include "ofMain.h"
#include "ofxMidi.h"

class MyListener: public ofxMidiListener {
    int code;
    long started;
public:
    MyListener() { code = -1; }
    void newMidiMessage(ofxMidiMessage& evnt) {
        if (started + 500 < ofGetElapsedTimeMillis()) {
            if (evnt.status == MIDI_CONTROL_CHANGE) {
                code = evnt.control;
            }
        }
    }

    int wait() {
        started = ofGetElapsedTimeMillis();
        while (code < 0) {
            ofSleepMillis(100);
        }
        cout << code << endl;
        return code;
    }
};

//========================================================================
int main( ){
    MyListener lst;
    ofxMidiIn* pMidiIn = new ofxMidiIn();
    int nmid = pMidiIn->getNumPorts();
    for (int i = 0; i < nmid; ++i) {
        if (i) {
            pMidiIn = new ofxMidiIn();
        }
        cout << pMidiIn->getPortName(i) << endl;
        pMidiIn->openPort(i);
        // Add this app as a listener.
        pMidiIn->addListener(&lst);
    }

    return nmid ? lst.wait() : 0;
}
